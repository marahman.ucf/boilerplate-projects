import { useEffect, useState } from 'react';

function FormInput(props) {
    const { id, placeholder, labelText, value, onChange, type } = props;

    return (
        <div className="mb-3">
            <label htmlFor={id} className="form-label">{labelText}</label>
            <input value={value} onChange={onChange} required type={type}
            className="form-control" id={id} placeholder={placeholder}/>
        </div>
    );
}

function AccountForm(props) {
    const [email, setEmail] = useState('');
    const [name, setName] = useState('');
    const [password, setPassword] = useState('');
    const [states, setStates] = useState([]);

    useEffect(() => {
        async function getStates () {
            const url = `${process.env.REACT_APP_API}/api/states`;
            const response = fetch(url);
            if (response.ok) {
                const data = (await response).json();
                setStates(data);
            }
        }
        getStates();
    }, [setStates])

    return (
        <form>
            <FormInput
                id="email"
                placeholder="you@example.com"
                labelText="Your Email"
                value={email}
                onChange={e => setEmail(e.target.value)}
                type="email"
            />
            <FormInput
                id="name"
                placeholder="John Smith"
                labelText="Your Name"
                value={name}
                onChange={e => setName(e.target.value)}
                type="text"
            />
            <FormInput
                id="password"
                placeholder="Secret"
                labelText="Your Password"
                value={password}
                onChange={e => setPassword(e.target.value)}
                type="password"
            />
            <div className="mb-3">
                <label htmlFor="states" className="form-label">Choose Your State</label>
                <select required className="form-select" id="states" aria-label="Choose Your State">
                    <option value="">Your State</option>
                    {states.map(state => (
                        <option key={state.abbreviation} value={state.abbreviation}>
                            {state.name}
                        </option>
                    ))}
                </select>
            </div>
            <button disabled={states.length === 0} type="submit" className="btn btn-primary">Submit</button>
        </form>
    );
}

export default AccountForm;
